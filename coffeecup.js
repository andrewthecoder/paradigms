
class CoffeeCup {
  constructor() {
    this.isEmpty = true;
  }

  refill() {
    this.isEmpty = false;
    console.log("Coffee cup refilled!");
  }

  drink() {
    if (this.isEmpty) {
      console.log("The coffee cup is empty. Please refill.");
    } else {
      console.log("Drinking from the coffee cup.");
      this.isEmpty = true;
    }
  }
}

// usage:
const myCoffeeCup = new CoffeeCup();
myCoffeeCup.drink(); // "The coffee cup is empty. Please refill."
myCoffeeCup.refill(); // "Coffee cup refilled!"
myCoffeeCup.drink(); // "Drinking from the coffee cup."
myCoffeeCup.drink(); // "The coffee cup is empty. Please refill."
